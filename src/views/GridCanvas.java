package views;

import controller.GameController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

public class GridCanvas extends JPanel {

    ArrayList<int[]> cells = new ArrayList<>();
    int canvasSize;
    boolean showGrid = true;
    GameController controller;

    public GridCanvas(int size, GameController gameController){
        canvasSize = size;
        controller = gameController;
        setBackground(Color.WHITE);
        addMouseListener(new CustomMousListener());
        setPreferredSize(new Dimension(size, size));
        setVisible(true);
    }

    public void setCells(ArrayList<int[]> newCells){
        cells = newCells;
    }

    public void setGrid(boolean visible){
        showGrid = visible;
        repaint();
    }

    public void alterCell(int x, int y){
        controller.alterCell(x, y);
    }

    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);

        if(showGrid){
            g.setColor(new Color(189, 195, 199));
            // vertical
            for (int i = 0; i < getSize().height; i += 10){
                g.drawLine(0, i, getSize().width, i);
            }

            // horizontal
            for (int i = 0; i < getSize().width; i += 10){
                g.drawLine(i, 0, i, getSize().height);
            }
        }

        g.setColor(new Color(22, 160, 133));
        for(int[] cell : cells){
            g.fillRect(cell[0]*10, cell[1]*10,10,10);
            // g.fillOval(cell[0]*10, cell[1]*10,10,10);
        }
    }

    class CustomMousListener implements MouseListener{

        @Override
        public void mouseClicked(MouseEvent mouseEvent) {
            alterCell(mouseEvent.getX(), mouseEvent.getY());
        }

        @Override
        public void mousePressed(MouseEvent mouseEvent) {}

        @Override
        public void mouseReleased(MouseEvent mouseEvent) {}

        @Override
        public void mouseEntered(MouseEvent mouseEvent) {}

        @Override
        public void mouseExited(MouseEvent mouseEvent) {}
    }
}
