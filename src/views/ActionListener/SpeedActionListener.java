package views.ActionListener;

import controller.GameController;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class SpeedActionListener implements ChangeListener {

        GameController controller;
        JSlider slider;

        public SpeedActionListener(GameController controller, JSlider slider) {
            this.controller = controller;
            this.slider = slider;
        }

        @Override
        public void stateChanged(ChangeEvent changeEvent) {
            controller.setSleep(slider.getValue());
        }
}
