package views.ActionListener;

import controller.GameController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class StartActionListener implements ActionListener {

    GameController controller;

    public StartActionListener(GameController controller){
        this.controller = controller;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        controller.play();
    }
}