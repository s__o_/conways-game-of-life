package views.ActionListener;

import controller.GameController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/*
* This part is not fully implimenten yet.
*/

public class ResetActionListener implements ActionListener {

        GameController controller;

        public ResetActionListener(GameController controller) {
            this.controller = controller;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            controller.play();
        }
}
