package views.ActionListener;

import controller.GameController;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class GridChangeListener implements ChangeListener {

    GameController controller;

    public GridChangeListener(GameController controller){
        this.controller = controller;
    }

    @Override
    public void stateChanged(ChangeEvent changeEvent) {
        controller.setGrid();
    }

}
