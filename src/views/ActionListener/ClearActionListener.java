package views.ActionListener;

import controller.GameController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ClearActionListener implements ActionListener {

    GameController controller;

    public ClearActionListener(GameController controller){
        this.controller = controller;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        this.controller.reset();
    }
}
