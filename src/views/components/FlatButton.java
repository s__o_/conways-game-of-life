package views.components;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;

public class FlatButton extends JButton {

    public FlatButton(String label){
        super(label);

        setBackground(new Color(255, 255, 255));
        setMargin(new Insets(5,5,5,5));
        setPreferredSize(new Dimension(30, 30));
        setBorderPainted(false);
        setFocusPainted(false);

        addMouseListener(new java.awt.event.MouseListener() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {

            }

            @Override
            public void mousePressed(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseEntered(MouseEvent mouseEvent) {
                setBackground(new Color(165, 177, 194));
            }

            @Override
            public void mouseExited(MouseEvent mouseEvent) {
                setBackground(new Color(255, 255, 255));
            }
        });
    }
}

