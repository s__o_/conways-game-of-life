package views.components;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;

public class ReplayButton extends FlatButton{

    Image icon;

    public ReplayButton(String label){
        super(label);

        try{
            icon = ImageIO.read(getClass().getResource("/assets/repeat.png"));
            setIcon(new ImageIcon(icon));
        }catch (Exception ex){
            System.out.println(ex.getStackTrace());
        }
    }
}
