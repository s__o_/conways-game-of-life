package views.components;

import controller.GameController;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;

public class ClearButton extends FlatButton{

    GameController controller;

    public ClearButton(String label){
        super(label);

        try{
            Image icon = ImageIO.read(getClass().getResource("/assets/trash.png"));
            setIcon(new ImageIcon(icon));
        }catch (Exception ex){
            System.out.println(ex.getStackTrace());
        }
    }
}
