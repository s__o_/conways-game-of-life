package views.components;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class PlayButton extends FlatButton {

    Image play;
    Image stop;

    public PlayButton(String label){
        super(label);


        try {
            play = ImageIO.read(getClass().getResource("/assets/caret-right.png"));
            stop = ImageIO.read(getClass().getResource("/assets/square.png"));

            setIcon(new ImageIcon(play));
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    public void alterImage(boolean playing){
        if(playing){
            setIcon(new ImageIcon(stop));
        }else{
            setIcon(new ImageIcon(play));
        }
    }
}
