package views;

import controller.GameController;
import views.ActionListener.*;
import views.components.ClearButton;
import views.components.PlayButton;
import views.components.ReplayButton;
import views.components.Slider;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class Board extends JFrame implements BoardView{

    GridCanvas canvas;

    Slider slider;
    JLabel speed;
    JLabel epoch;
    PlayButton playButton;
    ReplayButton resetButton;
    ClearButton clearButton;

    JCheckBox showGrid;
    GameController controller;
    JPanel controls;

    boolean playing = false;

    public Board(int size, GameController gameController){
        controller = gameController;

        controls = new JPanel();
        controls.setLayout(new FlowLayout());

        playButton = new PlayButton("");
        playButton.addActionListener(new StartActionListener(controller));

        // TODO: replay button not implemented yet.
        resetButton = new ReplayButton("");
        resetButton.addActionListener(new ResetActionListener(controller));
        resetButton.setEnabled(false);

        clearButton = new ClearButton("");
        clearButton.addActionListener(new ClearActionListener(controller));


        slider = new Slider();
        slider.setMinimum(0);
        slider.setMaximum(10);
        slider.setValue(1);
        slider.addChangeListener(new SpeedActionListener(controller, slider));


        speed = new JLabel("Speed: " + slider.getValue());

        showGrid = new JCheckBox();
        showGrid.setText("Grid");
        showGrid.setSelected(true);
        showGrid.addChangeListener(new GridChangeListener(controller));

        epoch = new JLabel();
        epoch.setText("Epoch: 0");

        controls.add(playButton);
        controls.add(resetButton);
        controls.add(clearButton);
        controls.add(speed);
        controls.add(slider);
        controls.add(showGrid);
        controls.add(epoch);

        getContentPane().add(controls, BorderLayout.NORTH);

        canvas = new GridCanvas(size, controller);
        getContentPane().add(canvas, BorderLayout.CENTER);

        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Game of Life");
        setResizable(false);
        setVisible(true);
    }

    public void setEpoch(int newEpoch){
        epoch.setText("Epoch: " + newEpoch);
    }

    @Override
    public void update(ArrayList<int[]> livingCells) {
        canvas.setCells(livingCells);
        canvas.repaint();
    }

    @Override
    public void stop(){
        resetButton.setEnabled(false);
        playButton.alterImage(false);
        clearButton.setEnabled(true);
    }

    @Override
    public void start(){
        playButton.alterImage(true);
        resetButton.setEnabled(true);
        clearButton.setEnabled(false);
    }

    public void setSpeed(int speedLevel){
        this.speed.setText("Speed: " +speedLevel);
    }

    public void setShowGrid(){
        canvas.setGrid(showGrid.isSelected());
    }
}
