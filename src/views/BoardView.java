package views;

import models.Grid;

import java.util.ArrayList;

public interface BoardView {

    // void setGrid(Grid grid);

    void update(ArrayList<int[]> livingCells);

    void start();

    void stop();
}
