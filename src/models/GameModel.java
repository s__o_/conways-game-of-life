package models;

public interface GameModel {

    void init(int size);

    void print();

    void envolve();
}
