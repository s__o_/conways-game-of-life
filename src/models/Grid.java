package models;

import java.util.ArrayList;

public class Grid implements GameModel{

    boolean[][] cells;
    int gridSize;

    @Override
    public void init(int size) {
        cells = new boolean[size][size];
        gridSize = size;

        for(boolean[] row : cells){
            for(boolean cell : row) {
                cell = false;
            }
        }

        // default start marking
        cells[13][14] = true;
        cells[14][13] = true;
        cells[14][14] = true;
        cells[14][15] = true;
        cells[15][14] = true;
    }

    @Override
    public void envolve(){
        boolean[][] newCells = new boolean[gridSize][gridSize];
        for (int i= 0; i < cells.length; i++){
            for(int j = 0; j < cells.length; j++){
                newCells[i][j] = isAlive(i, j);
            }
        }
        cells = newCells;
    }

    private boolean isAlive(int i, int j){
        boolean currentlyAlive = cells[i][j];
        int livingNeighbours = getLivingNeighbours(i, j);

        if((livingNeighbours == 2 || livingNeighbours == 3) && currentlyAlive){
            return true;
        } else if(!currentlyAlive && livingNeighbours == 3){
            return true;
        } else {
            return false;
        }
    }

    private int getLivingNeighbours(int i, int j){
        int livingNeighbours = 0;

        for(int x = -1; x < 2; x++){
            for(int y = -1; y < 2; y++){
                /*
                x = 0 and y = 0 -> false
                i + x < 0 oder i + x > size -> false
                j + y < 0 oder j + y > size -> false
                 */
                if(x == 0 && y == 0){

                }else if(i + x < 0 || i + x > gridSize - 1){

                }else if(j + y < 0 || j + y > gridSize - 1){

                }else if(cells[i+x][j+y]){
                    livingNeighbours += 1;
                }
            }
        }
        return livingNeighbours;
    }

    @Override
    public void print(){
        for (boolean[] cell : cells) {
            for (int j = 0; j < cells.length; j++) {
                if (cell[j]) {
                    System.out.print(" + ");
                } else {
                    System.out.print("   ");
                }
            }
            System.out.println();
        }
    }

    public ArrayList<int[]> getLivingCells() {
        ArrayList<int[]> livingCells = new ArrayList<>();
        for(int i = 0; i < gridSize; i++){
            for (int j = 0; j < gridSize; j++){
                if (cells[i][j]){
                    int[] position = new int[2];
                    position[0] = i;
                    position[1] = j;
                    livingCells.add(position);
                }
            }
        }
        return livingCells;
    }

    public void alterCell(int x, int y) {
        cells[x][y] = !cells[x][y];
    }

    public void clear(){
        cells = new boolean[gridSize][gridSize];
    }
}
