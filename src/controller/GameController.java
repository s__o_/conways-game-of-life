package controller;

import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingWorker;

import models.Grid;
import views.Board;

public class GameController {

    Grid grid = new Grid();
    Board board;

    int sleep = 1000;
    SwingWorker worker;

    boolean playing = false;

    public GameController(){
        int size = 600;

        board = new Board(size, this);
        grid.init(size);
        board.update(grid.getLivingCells());
    }

    public void setSleep(int sleep){
        board.setSpeed(sleep);

        sleep = (int) ((sleep * (-0.2) + 1.2) * 1000);

        if(sleep < 10){
            sleep = 10;
        }
        this.sleep = sleep;
    }

    public void alterCell(int x, int y){
        x = (int) x / 10;
        y = (int) y / 10;
        grid.alterCell(x, y);
        board.update(grid.getLivingCells());
    }

    public void play(){

        this.playing = !this.playing;

        if(this.playing){

            board.start();

            worker = new SwingWorker() {
                @Override
                protected Object doInBackground() throws Exception {
                    int epoch = 0;
                    try{
                        while (true){
                            grid.envolve();
                            epoch += 1;
                            publish(epoch);
                            Thread.sleep(sleep);
                        }
                    }catch (Exception e){}
                    return null;
                }

                @Override
                protected void process(List chunks) {
                    super.process(chunks);
                    ArrayList<int[]> cells = grid.getLivingCells();
                    board.update(cells);
                    board.setEpoch((int) chunks.get(0));
                    if(cells.size() == 0) {
                        stop();
                    }
                }
            };
            worker.execute();
        }else{
            stop();
        }
    }

    public void stop(){
        worker.cancel(true);
        board.stop();
    }

    public void reset(){
        grid.clear();
        board.update(grid.getLivingCells());
    }

    public void setGrid(){
        board.setShowGrid();
    }
}
