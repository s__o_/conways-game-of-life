# Game of Life

![logo](./pictures/logo.png)

The well-know zero-player game '[Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)' simulates the evolution of a set of initial cells, following four simple rules. The rules decide whether a cell will be alive in the next epoch or not. Based on that, an initial population, the cells may evolve from epoch to epoch and eventually reach a stable state or ultimately die.


![demo](./pictures/game_of_life_480p.gif)

### Game Rules

Rules as copyed from [Wikipedia](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life):

1. Any live cell with fewer than two live neighbours dies, as if by underpopulation.
2. Any live cell with two or three live neighbours lives on to the next generation.
3. Any live cell with more than three live neighbours dies, as if by overpopulation.
4. Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.

## Features

This implementation supports the following functionality:

- create an initial population by drawing on the game canvas
- add or remove cells during the simulation via mouse events
- adapt the game speed to control the duration of the epochs



## Setup

This implementation was developed using `Java JDK 1.8`.
